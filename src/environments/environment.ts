// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  twilioConfig: {
    accountSid: "AC28bb8e3504c24f2c6add462dcc641ee9",
    authToken: "c28bb2900bc244e376e46e1648d4f632",
    sid: "SK6e5dfe552eab951c49fe5f1e95b9a0a8",
    secret: "yzr7OzemQCCrZOjKQZPQI5lTEztT72yB"
  },
  twilio_endpoint: 'https://api.zenith-qa.jobsvenue.us',
  hasura_endpoint: 'https://hasura.zenith-qa.jobsvenue.us/v1/graphql',
  ws_endpoint: 'wss://hasura.zenith-qa.jobsvenue.us/v1/graphql',
  cloud_messaging: 'https://fcm.googleapis.com/fcm/send',
  storage_endpoint: 'https://storage.zenith.jobsvenue.us',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
