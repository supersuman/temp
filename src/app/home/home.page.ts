import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { connect } from 'twilio-video';
import * as Video from 'twilio-video';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  meeting_id = '';
  meeting_user_id = '';
  meeting_user_name = '';

  audioDevices: any = [];
  videoDevices: any = [];

  currentAudioId: any;
  currentVideoId: any;

  audioEnabled = true;
  videoEnabled = true;

  meetDetails: any;

  room: any;
  remoteTracks: any = [];
  tempRemoteTracks: any = [];

  localMedia: any;

  callLifted: boolean = false;
  callConnected = false;

  localVideo: any;
  localTracks: any;

  callingUnit: any = null;
  caller_photo = '';

  roomSubject: Subject<any> = new Subject();

  time_elapsed: number = 0;
  time_string: string;
  timeInterval: any;

  rolling_call_lifted = false;
  currentCallId: any = '';
  subscribedCallValue: any = null;
  callEnded: boolean = false;
  insertCall: any;
  subscribeCall: any;

  token: any;
  room_name: any = '';


  constructor(private http: HttpClient) {
    this.call()
  }

  call() {
    this.room_name = uuid();
    console.log(this.room_name, 'room name');
    this.http.get(environment.twilio_endpoint + "/twilio/token?room_name=fjkdghsfksdu").subscribe(async (res: any) => {
      console.log(res, 'res');
      this.token = res.token;
      this.createLocalTracks();
    })
  }


  createLocalTracks(): any {
    // const room_name = this.callingResident.user_property_id;
    this.callEnded = false;
    this.audioDevices = [];
    this.videoDevices = [];
    const room_name = uuid();
    console.log(room_name, 'room_name');
    navigator.mediaDevices.enumerateDevices().then(async (mediaDevices) => {
      console.log(mediaDevices, 'mediaDevices1');
      mediaDevices.forEach((mediaDevice: any) => {
        if (mediaDevice.kind === 'videoinput') {
          console.log(mediaDevice.label);
          let obj = {
            label: mediaDevice.label,
            value: mediaDevice.deviceId
          };
          this.videoDevices.push(obj);
        }
        if (mediaDevice.kind === 'audioinput') {
          console.log(mediaDevice.label);
          let obj = {
            label: mediaDevice.label,
            value: mediaDevice.deviceId
          };
          this.audioDevices.push(obj);
        }
      });
      console.log(this.audioDevices, 'audio devices');
      console.log(this.videoDevices, 'video devices');
      this.currentAudioId = this.audioDevices[0].value;
      this.currentVideoId = this.videoDevices[0].value;
      console.log(this.currentAudioId, 'currentAudioId');
      console.log(this.currentVideoId, 'currentVideoId');
      if (!this.callEnded) {
        const room = await Video.createLocalTracks({
          audio: { deviceId: this.currentAudioId },
          video: { deviceId: this.currentVideoId }
        }).then(localTracks => {
          var localMediaContainer = document.getElementById('local-media');
          this.localMedia = localTracks;
          localTracks.forEach(function (track: any) {
            localMediaContainer?.appendChild(track?.attach());
            let video = document?.getElementsByTagName('video');
            for (let i = 0; i < video.length; i++) {
              video[i].style.height = '100%';
              video[i].style.width = '100%';
              video[i].style.borderRadius = '20px';
              video[i].style.objectFit = 'contain';
            }
          });
          return Video.connect(this.token, {
            name: this.meeting_id,
            tracks: localTracks
          });
        });
        this.room = room;
        this.roomSubject.next(room);
        console.log(`Connected to Room: ${room.name} with sid: ${room.sid}`);
        console.log(room, 'room');
        room.on('participantConnected', participant => {
          console.log(`Participant "${participant.identity}" connected`);
          document.getElementById('local-media').style.opacity = '100%';
          navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
            const video = document.createElement('video');
            video.srcObject = stream;
            video.onloadedmetadata = (e) => {
              video.play();
              const canvas = document.createElement('canvas');
              canvas.width = 640;
              canvas.height = 480;
              const ctx = canvas.getContext('2d');
              ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
              const dataURL = canvas.toDataURL('image/png');
              console.log(dataURL);
              this.caller_photo = dataURL;
              stream.getTracks().forEach(track => track.stop());
            };
          });
          this.timeInterval = setInterval(() => {
            this.time_elapsed++;
          }, 1000);

          this.callLifted = true;
          // document.getElementById('local-media').style.transform = 'translate(80%, -210%)';
          document.getElementById('local-media').style.margin = '-47% 0 0 60%';
          participant.on('trackDisabled', track => {
            // console.log(`Track "${track.kind}" disabled`);
            console.log(track, 'track......');
            const remote = Array.from(document.getElementById('remote-media').children as HTMLCollectionOf<HTMLElement>);
            for (let i = 0; i < remote.length; i++) {
              if (track.kind === 'video' && remote[i].tagName === 'VIDEO') {
                console.log(remote[i].tagName, 'in trackdisabled');
                remote[i].style.display = 'none';
                const img = document.createElement('img');
                // set the source of the img node
                img.setAttribute('src', '/assets/call-no-circles.svg');
                document.getElementById('remote-media').appendChild(img);
              }
            }
            // create a img node
          });

          participant.on('trackEnabled', track => {
            // console.log(`Track "${track.kind}" enabled`);
            const remote = Array.from(document.getElementById('remote-media').children as HTMLCollectionOf<HTMLElement>);
            for (let i = 0; i < remote.length; i++) {
              if (remote[i].tagName === 'VIDEO' && track.kind === 'video') {
                console.log(remote[i].tagName, 'in trackenabled');
                remote[i].style.display = 'block';
                const img = document.getElementById('remote-media').getElementsByTagName('img')[0];
                img.remove();
              }
            }
          });

          participant.tracks.forEach(publication => {
            console.log(publication, 'publication');
            var track: any = publication.track;
            console.log(track, 'track on remote connnection');
            if (track?.name === 'screen') {
              console.log('screen share');
            }
            if (publication.isSubscribed) {
              var currentMedia = 'remote-media';
              this.remoteTracks.push(track);
              document?.getElementById('remote-media')?.appendChild(track?.attach());
              let video = document?.getElementsByTagName('video');
              for (let i = 0; i < video.length; i++) {
                video[i].style.height = '100%';
                video[i].style.width = '100%';
                video[i].style.borderRadius = '20px';
                video[i].style.objectFit = 'contain';
              }
            }
          });

          participant.on('trackSubscribed', (track: any) => {
            var currentMedia = 'remote-media';
            this.remoteTracks.push(track);
            document?.getElementById('remote-media')?.appendChild(track?.attach());
            let video = document?.getElementsByTagName('video');
            for (let i = 0; i < video.length; i++) {
              video[i].style.height = '100%';
              video[i].style.width = '100%';
              video[i].style.borderRadius = '20px';
              video[i].style.objectFit = 'contain';
            }
          });

          participant.on('trackUnsubscribed', (track: any) => {
            // if (track.name === 'screen') {
            //     console.log('screen share')
            //     this.screenShareMode = false;
            //     this.afterScreenShareEnded();
            // }
            track?.detach()?.forEach((element: any) => {
              console.log(element);
              element.remove();
            });
          });

          participant.on('trackUnpublished', (track: any) => {
            // if (track.name === 'screen') {
            //     console.log('screen share')
            //     this.screenShareMode = false;
            //     this.afterScreenShareEnded();
            // }
            track?.detach()?.forEach((element: any) => {
              element.remove();
            });
          });
        });
        room.on('participantDisconnected', participant => {
          console.log(participant);
          console.log(`Participant "${participant.identity}" has disconnected from the Room`);
          this.callLifted = false;
          this.callEnded = true;
          this.callingUnit = null;
          this.currentCallId = '';
          this.subscribedCallValue = null;
        });
        room.participants.forEach((participant) => {
          participant.tracks.forEach((publication: any) => {
            if (publication.track) {
              var currentMedia = 'remote-media';
              this.remoteTracks.push(publication?.track);
              document?.getElementById('remote-media')?.appendChild(publication?.track?.attach());
              let video = document?.getElementsByTagName('video');
              for (let i = 0; i < video.length; i++) {
                video[i].style.height = '100%';
                video[i].style.width = '100%';
                video[i].style.borderRadius = '20px';
                video[i].style.objectFit = 'contain';
              }
              publication.track.on('disabled', () => {
                console.log(publication.track);
                // if (this.screenShareMode) {

                // } else {
                //     console.log(publication.track)
                // }
              });
            }
          });

          participant.on('trackSubscribed', (track: any) => {
            var currentMedia = 'remote-media';
            this.remoteTracks.push(track);
            document?.getElementById('remote-media')?.appendChild(track?.attach());
            let video = document?.getElementsByTagName('video');
            for (let i = 0; i < video.length; i++) {
              video[i].style.height = '100%';
              video[i].style.width = '100%';
              video[i].style.borderRadius = '20px';
              video[i].style.objectFit = 'contain';
            }
          });

          participant.on('trackUnsubscribed', (track: any) => {
            track.detach().forEach((element: any) => {
              element.remove();
            });
          });

          participant.on('trackUnpublished', (publication: any) => {
            publication.track.detach().forEach((element: any) => {
              element.remove();
            });
          });
        });
        room.on('disconnected', room => {
          // Detach the local media elements
          console.log(room, 'disconnected from room');
          this.timeInterval ? clearInterval(this.timeInterval) : null;
          room.participants.forEach((participant) => {
            participant.tracks.forEach((publication: any) => {
              if (publication.track) {
                publication.track.detach().forEach((element: any) => {
                  element.remove();
                });
              }
            });
          });
          this.localMedia.forEach((element: any) => {
            element.stop();
          });
          room.localParticipant.tracks.forEach((publication: any) => {
            const attachedElements = publication.track?.detach();
            console.log(attachedElements, 'attachedElements');
            attachedElements?.forEach((element: any) => element.remove());
          });
          this.callLifted = false;
          this.callEnded = true;
          this.callingUnit = null;
          this.currentCallId = '';
          this.subscribedCallValue = null;
          console.log(room);
        });
      }
    });
    // this.http.get(environment.twilio_endpoint + "/twilio/token?room_name=" + room_name).subscribe(async (res: any) => {
    //   this.callConnected = true;
    //   document.getElementById('local-media').style.opacity = '0%';
    //   try {
    //     this.insertCall(this.callingUnit.unit_id).subscribe((insert_call_res: any) => {
    //       this.currentCallId = insert_call_res.data.insert_calls_one.call_id || '';
    //       this.subscribeCall(this.currentCallId);
    //       navigator.mediaDevices.enumerateDevices().then(async (mediaDevices) => {
    //         console.log(mediaDevices, 'mediaDevices1');
    //         mediaDevices.forEach((mediaDevice: any) => {
    //           if (mediaDevice.kind === 'videoinput') {
    //             console.log(mediaDevice.label);
    //             let obj = {
    //               label: mediaDevice.label,
    //               value: mediaDevice.deviceId
    //             };
    //             this.videoDevices.push(obj);
    //           }
    //           if (mediaDevice.kind === 'audioinput') {
    //             console.log(mediaDevice.label);
    //             let obj = {
    //               label: mediaDevice.label,
    //               value: mediaDevice.deviceId
    //             };
    //             this.audioDevices.push(obj);
    //           }
    //         });
    //         console.log(this.audioDevices, 'audio devices');
    //         console.log(this.videoDevices, 'video devices');
    //         this.currentAudioId = this.audioDevices[0].value;
    //         this.currentVideoId = this.videoDevices[0].value;
    //         console.log(this.currentAudioId, 'currentAudioId');
    //         console.log(this.currentVideoId, 'currentVideoId');
    //         if (!this.callEnded) {
    //           const room = await Video.createLocalTracks({
    //             audio: { deviceId: this.currentAudioId },
    //             video: { deviceId: this.currentVideoId }
    //           }).then(localTracks => {
    //             var localMediaContainer = document.getElementById('local-media');
    //             this.localMedia = localTracks;
    //             localTracks.forEach(function (track: any) {
    //               localMediaContainer?.appendChild(track?.attach());
    //               let video = document?.getElementsByTagName('video');
    //               for (let i = 0; i < video.length; i++) {
    //                 video[i].style.height = '100%';
    //                 video[i].style.width = '100%';
    //                 video[i].style.borderRadius = '20px';
    //                 video[i].style.objectFit = 'contain';
    //               }
    //             });
    //             return Video.connect(res.token, {
    //               name: this.meeting_id,
    //               tracks: localTracks
    //             });
    //           });
    //           this.room = room;
    //           this.roomSubject.next(room);
    //           console.log(`Connected to Room: ${room.name} with sid: ${room.sid}`);
    //           console.log(room, 'room');
    //           room.on('participantConnected', participant => {
    //             console.log(`Participant "${participant.identity}" connected`);
    //             document.getElementById('local-media').style.opacity = '100%';
    //             navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
    //               const video = document.createElement('video');
    //               video.srcObject = stream;
    //               video.onloadedmetadata = (e) => {
    //                 video.play();
    //                 const canvas = document.createElement('canvas');
    //                 canvas.width = 640;
    //                 canvas.height = 480;
    //                 const ctx = canvas.getContext('2d');
    //                 ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
    //                 const dataURL = canvas.toDataURL('image/png');
    //                 console.log(dataURL);
    //                 this.caller_photo = dataURL;
    //                 stream.getTracks().forEach(track => track.stop());
    //               };
    //             });
    //             this.timeInterval = setInterval(() => {
    //               this.time_elapsed++;
    //             }, 1000);

    //             this.callLifted = true;
    //             // document.getElementById('local-media').style.transform = 'translate(80%, -210%)';
    //             document.getElementById('local-media').style.margin = '-47% 0 0 60%';
    //             participant.on('trackDisabled', track => {
    //               // console.log(`Track "${track.kind}" disabled`);
    //               console.log(track, 'track......');
    //               const remote = Array.from(document.getElementById('remote-media').children as HTMLCollectionOf<HTMLElement>);
    //               for (let i = 0; i < remote.length; i++) {
    //                 if (track.kind === 'video' && remote[i].tagName === 'VIDEO') {
    //                   console.log(remote[i].tagName, 'in trackdisabled');
    //                   remote[i].style.display = 'none';
    //                   const img = document.createElement('img');
    //                   // set the source of the img node
    //                   img.setAttribute('src', '/assets/call-no-circles.svg');
    //                   document.getElementById('remote-media').appendChild(img);
    //                 }
    //               }
    //               // create a img node
    //             });

    //             participant.on('trackEnabled', track => {
    //               // console.log(`Track "${track.kind}" enabled`);
    //               const remote = Array.from(document.getElementById('remote-media').children as HTMLCollectionOf<HTMLElement>);
    //               for (let i = 0; i < remote.length; i++) {
    //                 if (remote[i].tagName === 'VIDEO' && track.kind === 'video') {
    //                   console.log(remote[i].tagName, 'in trackenabled');
    //                   remote[i].style.display = 'block';
    //                   const img = document.getElementById('remote-media').getElementsByTagName('img')[0];
    //                   img.remove();
    //                 }
    //               }
    //             });

    //             participant.tracks.forEach(publication => {
    //               console.log(publication, 'publication');
    //               var track: any = publication.track;
    //               console.log(track, 'track on remote connnection');
    //               if (track?.name === 'screen') {
    //                 console.log('screen share');
    //               }
    //               if (publication.isSubscribed) {
    //                 var currentMedia = 'remote-media';
    //                 this.remoteTracks.push(track);
    //                 document?.getElementById('remote-media')?.appendChild(track?.attach());
    //                 let video = document?.getElementsByTagName('video');
    //                 for (let i = 0; i < video.length; i++) {
    //                   video[i].style.height = '100%';
    //                   video[i].style.width = '100%';
    //                   video[i].style.borderRadius = '20px';
    //                   video[i].style.objectFit = 'contain';
    //                 }
    //               }
    //             });

    //             participant.on('trackSubscribed', (track: any) => {
    //               var currentMedia = 'remote-media';
    //               this.remoteTracks.push(track);
    //               document?.getElementById('remote-media')?.appendChild(track?.attach());
    //               let video = document?.getElementsByTagName('video');
    //               for (let i = 0; i < video.length; i++) {
    //                 video[i].style.height = '100%';
    //                 video[i].style.width = '100%';
    //                 video[i].style.borderRadius = '20px';
    //                 video[i].style.objectFit = 'contain';
    //               }
    //             });

    //             participant.on('trackUnsubscribed', (track: any) => {
    //               // if (track.name === 'screen') {
    //               //     console.log('screen share')
    //               //     this.screenShareMode = false;
    //               //     this.afterScreenShareEnded();
    //               // }
    //               track?.detach()?.forEach((element: any) => {
    //                 console.log(element);
    //                 element.remove();
    //               });
    //             });

    //             participant.on('trackUnpublished', (track: any) => {
    //               // if (track.name === 'screen') {
    //               //     console.log('screen share')
    //               //     this.screenShareMode = false;
    //               //     this.afterScreenShareEnded();
    //               // }
    //               track?.detach()?.forEach((element: any) => {
    //                 element.remove();
    //               });
    //             });
    //           });
    //           room.on('participantDisconnected', participant => {
    //             console.log(participant);
    //             console.log(`Participant "${participant.identity}" has disconnected from the Room`);
    //             this.callLifted = false;
    //             this.callEnded = true;
    //             this.callingUnit = null;
    //             this.currentCallId = '';
    //             this.subscribedCallValue = null;
    //           });
    //           room.participants.forEach((participant) => {
    //             participant.tracks.forEach((publication: any) => {
    //               if (publication.track) {
    //                 var currentMedia = 'remote-media';
    //                 this.remoteTracks.push(publication?.track);
    //                 document?.getElementById('remote-media')?.appendChild(publication?.track?.attach());
    //                 let video = document?.getElementsByTagName('video');
    //                 for (let i = 0; i < video.length; i++) {
    //                   video[i].style.height = '100%';
    //                   video[i].style.width = '100%';
    //                   video[i].style.borderRadius = '20px';
    //                   video[i].style.objectFit = 'contain';
    //                 }
    //                 publication.track.on('disabled', () => {
    //                   console.log(publication.track);
    //                   // if (this.screenShareMode) {

    //                   // } else {
    //                   //     console.log(publication.track)
    //                   // }
    //                 });
    //               }
    //             });

    //             participant.on('trackSubscribed', (track: any) => {
    //               var currentMedia = 'remote-media';
    //               this.remoteTracks.push(track);
    //               document?.getElementById('remote-media')?.appendChild(track?.attach());
    //               let video = document?.getElementsByTagName('video');
    //               for (let i = 0; i < video.length; i++) {
    //                 video[i].style.height = '100%';
    //                 video[i].style.width = '100%';
    //                 video[i].style.borderRadius = '20px';
    //                 video[i].style.objectFit = 'contain';
    //               }
    //             });

    //             participant.on('trackUnsubscribed', (track: any) => {
    //               track.detach().forEach((element: any) => {
    //                 element.remove();
    //               });
    //             });

    //             participant.on('trackUnpublished', (publication: any) => {
    //               publication.track.detach().forEach((element: any) => {
    //                 element.remove();
    //               });
    //             });
    //           });
    //           room.on('disconnected', room => {
    //             // Detach the local media elements
    //             console.log(room, 'disconnected from room');
    //             this.timeInterval ? clearInterval(this.timeInterval) : null;
    //             room.participants.forEach((participant) => {
    //               participant.tracks.forEach((publication: any) => {
    //                 if (publication.track) {
    //                   publication.track.detach().forEach((element: any) => {
    //                     element.remove();
    //                   });
    //                 }
    //               });
    //             });
    //             this.localMedia.forEach((element: any) => {
    //               element.stop();
    //             });
    //             room.localParticipant.tracks.forEach((publication: any) => {
    //               const attachedElements = publication.track?.detach();
    //               console.log(attachedElements, 'attachedElements');
    //               attachedElements?.forEach((element: any) => element.remove());
    //             });
    //             this.callLifted = false;
    //             this.callEnded = true;
    //             this.callingUnit = null;
    //             this.currentCallId = '';
    //             this.subscribedCallValue = null;
    //             console.log(room);
    //           });
    //         }
    //       });
    //     }, (err: any) => {
    //       console.log(err, 'err while creating call');
    //     });
    //   } catch (err) {
    //     console.log(err, 'err in try catch');
    //   }
    // }, (err: any) => {
    //   console.log(err);
    // });
  }

}

